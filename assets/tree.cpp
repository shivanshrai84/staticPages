// Given a vector based representation of edges of a tree, construct it (line
// 93). On this tree, a set of queries will be performed. Each query contains
// the index of a node, and the number of nodes (rooted at the subtree of this
// node) having prime values has to be returned.

#include <iostream>
#include <unordered_map>
#include <unordered_set>
#include <vector>
using namespace std;

struct TreeNode {
    int val;
    int index;
    int primeCnt;
    vector<TreeNode *> nbrs;  // neighbours
    TreeNode(int v, int i) {
        val = v;
        index = i;
        primeCnt = 0;
    }
};

int countPrimes(int index, TreeNode *root, TreeNode *parent,
                vector<bool> &prime, unordered_map<int, int> &m) {
    if (m.find(index) != m.end()) return m[index];
    int count = 0;
    if (prime[root->val]) {
        count++;
    }
    for (auto child : root->nbrs) {
        if (child == parent) continue;
        if (m.find(child->index) != m.end()) {
            count += m[child->index];
        } else {
            count += countPrimes(child->index, child, root, prime, m);
        }
    }
    m[index] = count;
    root->primeCnt = count;
    return count;
}

// Complete the primeQuery function below.
vector<int> primeQuery(int n, vector<int> first, vector<int> second,
                       vector<int> values, vector<int> queries) {
    unordered_set<int> visited;
    unordered_map<int, int> m;
    vector<TreeNode *> ptrs(n);
    vector<int> result, primeCount;
    int maxVal = 0;

    for (const auto &i : values) {
        maxVal = max(maxVal, i);
    }

    // Generate first maxVal primes using sieve of Eratosthenes.
    vector<bool> prime(maxVal + 1, true);
    for (int i = 2; i * i <= maxVal; i++) {
        if (prime[i]) {
            for (int j = i * 2; j <= maxVal; j += i) {
                prime[j] = false;
            }
        }
    }

    for (int i = 0; i < first.size(); i++) {
        first[i]--;
        second[i]--;
        if (visited.find(first[i]) == visited.end()) {
            ptrs[first[i]] = new TreeNode(values[first[i]], first[i]);
            visited.insert(first[i]);
        }
        if (visited.find(second[i]) == visited.end()) {
            ptrs[second[i]] = new TreeNode(values[second[i]], second[i]);
            visited.insert(second[i]);
        }
        // update neighbours
        ptrs[first[i]]->nbrs.push_back(ptrs[second[i]]);
        ptrs[second[i]]->nbrs.push_back(ptrs[first[i]]);
    }

    countPrimes(ptrs[0]->index, ptrs[0], NULL, prime, m);

    for (const auto &i : queries) {
        result.push_back(m[ptrs[i - 1]->index]);
    }

    return result;
}

int main() {
    // first(i) and second(i) represent end points of an edge.
    vector<int> first = {6, 8, 3, 6, 4, 1, 8, 5, 1};
    vector<int> second = {9, 9, 5, 7, 8, 8, 10, 8, 2};
    vector<int> values = {17, 29, 3, 20, 11, 8, 3, 23, 5, 15};
    vector<int> queries = {1, 2, 3, 4};
    vector<int> result = primeQuery(10, first, second, values, queries);
    for (const auto &i : result) {
        cout << i << " ";
    }
    cout << endl;
    return 0;
}
