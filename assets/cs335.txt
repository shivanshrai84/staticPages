* https://stackoverflow.com/questions/8422146/go-how-to-create-a-parser
* Scanning and tokens - https://medium.com/@farslan/a-look-at-go-scanner-packages-11710c2655fc
* http://www.colm.net/open-source/ragel/

## Packages (for lexical analysis and parsing)
* Scanner - https://golang.org/pkg/go/scanner
* Parser - https://golang.org/pkg/go/parser
* token - https://golang.org/pkg/go/token
* lexer - https://github.com/cznic/golex
* yacc samples - https://github.com/golang-samples/yacc

## IR
* https://groups.google.com/forum/#!topic/golang-nuts/pNbG9TJDXkk
* SSA

## Scoping in Go
Scoping in go is different, due to `:=` ; this can be added as a feature which will be added in the symbol table.

# Lib
## Hashtable
* http://www.linuxjournal.com/content/hash-tables%E2%80%94theory-and-practice

## Concurrency
* https://github.com/golang/go/wiki/LearnConcurrency

- - -

# Go internals
* https://blog.altoros.com/golang-part-1-main-concepts-and-project-structure.html

## Segmented stacks
* https://blog.cloudflare.com/how-stacks-are-handled-in-go/
* https://groups.google.com/forum/#!topic/golang-dev/i7vORoJ3XIw
* https://docs.google.com/document/d/1wAaf1rYoM4S4gtnPh0zOlGzWtrZFQ5suE8qr2sD8uWQ/pub

## Design of go-fmt
Generates an AST from the source, reformats accordingly, and writes the generated (formatted) source code back.

## Lec
* 28: Code generation
