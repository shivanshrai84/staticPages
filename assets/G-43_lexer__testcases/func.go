// foo ()
// {
//     int i = 1;
// }
//
// int main(){
//     foo();
//     return 0;
// }

package main

func foo() {
	var i int = 1
}

func main() {
	foo()
	return // return 0 ??
}
