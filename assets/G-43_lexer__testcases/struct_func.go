// typedef struct
// {
//   char a;
//   int b;
//   char c;
//   short d;
//   double e;
//   char name[10];
//   char f;
// } T;

// void f (T x)
// {
//   x.a = 'a';
//   x.b = 47114711;
//   x.c = 'c';
//   x.d = 1234;
//   x.e = 3.141592897932;
//   x.f = '*';
//   x.name = "abc";
// }

// int main (){
//     T k;
//     f(k);
//     return 0;
// }

package main

type T struct {
	a    byte
	b    int
	c    byte
	d    int16
	e    float64
	name string
	f    byte
}

func f(x T) {
	x.a = 'a'
	x.b = 47114711
	x.c = 'c'
	x.d = 1234
	x.e = 3.141592897932
	x.f = '*'
	x.name = "abc"
}

func main() {
	var k T
	f(k)
	return // return 0 ??
}
