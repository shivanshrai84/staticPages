* book vs level ; wallet ; trades
* what happens once matched ?
* [doc] where there are M orders allowed for each price fluctuation in the direct contingent address space

~~~~~~~~~~~~~~~~

The aim of the matching engine, and all processes reading the market information is to have as few memory cache misses as possible.

This is not a process which clients can directly interact with. Only other internal processes will send/receive data to the matching engine (that is, when a client sends an incoming order, that incoming order is sent to a process which validates the order with the client's account before sending to the matching engine).

For each market, there needs to be a relatively coarse minimum price fluctuation which can be modified over time. For markets such as BTC/fiat, this is generally not an issue.. that is, BTC/USD can be priced in one cent increments. But LTC/BTC can be 8 decimal places. It would make things easier to program, and easier for traders, if we limit those number of decimal places to fluctuations which would be commonly used. The fluctuation configuration will have to be determined as needed.

In mmap'ed RAM (so that other processes can see the market), the orders will generally look like...
...
[Sell price-2 Order 1]
[Sell price-2 Order 2]
[Sell price-2 Order 3]
.. [allow M such orders]
[Sell price-1 Order 1]
[Sell price-1 Order 2]
[Sell price-1 Order 3]
.. [allow M such orders]
[Buy price-1 Order 1]
[Buy price-1 Order 2]
[Buy price-1 Order 3]
.. [allow M such orders]
[Buy price-2 Order 1]
[Buy price-2 Order 2]
[Buy price-2 Order 3]
.. [allow M such orders]
...
where there are M orders allowed for each price fluctuation in the direct contingent address space (M to also be modified as needed). If there are more than M orders, those extra orders are listed elsewhere in memory (as an overflow-type of listing). It is the contiguous address space which allows for the speed of 1) the matching, and 2) other processes to read off the market.
For example, a market buy order starts at the lowest address and works its way up until completely filled, rarely having a cache miss to divert to fill orders when there are over M orders listed at a price level.
